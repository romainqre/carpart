# How to build and start Dockerfile's

## Build (only once)

#### Build Database:

```
docker build -t stocks-db -f Dockerfile.db .
```

#### Build API:

```
docker build -t stocks-api -f Dockerfile.api .
```

## Run

#### Run Database:

```
docker run --name stocks-db -p 27017:27017 stocks-db
```

#### Run API:

```
docker run --name stocks-api -p 8000:8000 stocks-api
```

## Stop

Recup the PID with ```docker ps```.

```
docker stop PID
```

## Start

Recup the PID with ```docker ps```.

```
docker start PID
```