CREATE DATABASE IF NOT EXISTS carpart;
USE carpart;

-- Création de la table 'clients'
CREATE TABLE IF NOT EXISTS clients (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    prenom VARCHAR(255),
    email VARCHAR(255),
    nb_commandes INT
);