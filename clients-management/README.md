# How to build and start Dockerfile's

## Build (only once)

#### Build Database:

```
docker build -t clients-db -f Dockerfile.db .
```

#### Build API:

```
docker build -t clients-api -f Dockerfile.api .
```

## Run

#### Run Database:

```
docker run --name clients-db --network mynetwork -p 3307:3307 -e MYSQL_ROOT_PASSWORD=root clients-db
```

#### Run API:

```
docker run --name clients-api --network mynetwork -p 8001:8001 clients-api
```

## Stop

Recup the PID with ```docker ps```.

```
docker stop PID
```

## Start

Recup the PID with ```docker ps```.

```
docker start PID
```